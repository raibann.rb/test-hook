import React from "react";
import { Routes, Route, Link } from "react-router-dom";
import Addlist from "../pages/Addlist";
import Showlist from "../pages/Showlist";

// Style material
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";

// Routes
const AllRouters = (props) => {
  // const { prop1, prop2 } = props;
  console.log("Route Props:", props);

  return (
    <>
      <Stack spacing={2} direction="row">
        <Button variant="text">
          <Link
            to="/"
            style={{
              textDecoration: "none",
              color: "white",
              backgroundColor: "#1976d2",
              padding: "1rem",
            }}
          >
            Showlist
          </Link>
        </Button>
        <Button variant="text">
          <Link
            to="/addlist"
            style={{
              textDecoration: "none",
              color: "white",
              backgroundColor: "#1976d2",
              padding: "1rem",
            }}
          >
            Addlist
          </Link>
        </Button>
      </Stack>
      <Routes>
        <Route exact path="/" element={<Showlist data={props} />} />
        <Route path="/addlist" element={<Addlist add={props} />} />
        <Route path="/addlist/:id" element={<Addlist add={props} />} />
      </Routes>
    </>
  );
};
export default AllRouters;
