import React from "react";
import {useNavigate} from "react-router-dom"

//material ui
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";

const Showlist = ({ data }) => {
  console.log("Data:", data);
  // data.prop1;
  const onClickDeleteRow = (rw) => {
    let rowData = [...data.prop1];
    rowData = rowData.filter(function (obj) {
      return obj.id !== rw.id;
    });
    // prop2 is fuction
    data.prop2(rowData);
  };
  let navigate = useNavigate();
  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell align="center">First Name</TableCell>
              <TableCell align="center">Last Name</TableCell>
              <TableCell align="center">Phone Number</TableCell>
              <TableCell align="center">Email Address</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {data.prop1.map((row, index) => (
              <TableRow
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                key={row.id}
              >
                <TableCell align="center">{row.fname}</TableCell>
                <TableCell align="center">{row.lname}</TableCell>
                <TableCell align="center">{row.phone}</TableCell>
                <TableCell align="center">{row.email}</TableCell>
                <TableCell align="center">
                  <Button variant="text" onClick={() => navigate("/addlist/"+ row.id)}>Edit</Button>
                  <Button variant="text" onClick={() => onClickDeleteRow(row)}>
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default Showlist;
